package com.violentbits.ageo;

import static com.violentbits.ageo.Constants.ACTION_TIME_FAST;
import static com.violentbits.ageo.scene.ActionsEx.moveCameraAligned;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.violentbits.ageo.io.Assets;
import com.violentbits.ageo.io.GamePreferences;
import com.violentbits.ageo.scene.ConfirmDialog;
import com.violentbits.ageo.scene.GameChooser;
import com.violentbits.ageo.scene.MainMenu;
import com.violentbits.ageo.scene.OptionsWindow;

/**
 * This screen is responsible of the main menu and all the dialogs accessible from there.
 */
public class MenuScreen extends AbstractGameScreen {
	@SuppressWarnings("unused")
	private static final String TAG = MenuScreen.class.getName();
	private Stage stage;
	private Stage titleStage;
	private OptionsWindow winOptions;
	private GameChooser gameChooser;
	private MainMenu mainMenu;
	private ConfirmDialog confirmDialog;
	private Skin skin;
	
	private Array<Actor> actorStack = new Array<Actor>();
	
	private InputAdapter inputAdapter = new InputAdapter() {
		@Override
		public boolean keyUp(int keycode) {
			if (keycode == Keys.ESCAPE || keycode == Keys.BACK) {
				back();
			}
			return false;
		}
	};
	/**
	 * This is used to know which actor should be shown when the back
	 * button is pressed.
	 * 
	 * Call this when an actor in the main screen centers the view in
	 * another actor.
	 * 
	 * @param actor  The actor that will be visible if the back button is pressed.
	 */
	public void pushActor(Actor actor) {
		actorStack.add(actor);
	}
	/**
	 * Returns the last actor put in the stack using pushActor().
	 * 
	 * @return
	 */
	public Actor popActor() {
		return actorStack.pop();
	}
	/**
	 * Centers the camera on the last pushed actor. If the stack is empty, exits the application. 
	 */
	public void back() {
		if (actorStack.size==0)
			Gdx.app.exit();
		else {
			SoundController.instance.play(Assets.instance.getSound("select.wav"));
			Actor actor = popActor();
			actor.addAction(moveCameraAligned(Align.center, ACTION_TIME_FAST));
		}
	}

	public Stage getStage() {
		return stage;
	}
	
	public OptionsWindow getOptionsWindow() {
		return winOptions;
	}
	
	public GameChooser getGameChooser() {
		return gameChooser;
	}
	
	public MainMenu getMainMenu() {
		return mainMenu;
	}
	
	public ConfirmDialog getConfirmDialog() {
		return confirmDialog;
	}
	
	public Skin getSkin() {
		return skin;
	}
	
	private Actor buildTitle() {
		Image title = new Image(Assets.instance.get("apparat"));
		title.setPosition((titleStage.getWidth() - title.getWidth()) / 2f, titleStage.getHeight() * 0.8f);
		return title;
	}

	private Actor buildMenu() {
		mainMenu = new MainMenu(this, game);
		mainMenu.setPosition(
				(stage.getWidth() - mainMenu.getWidth()) / 2f,
				(stage.getHeight() - mainMenu.getHeight()) / 2f);
		return mainMenu;
	}
	
	private Actor buildConfirmDialog() {
		confirmDialog = new ConfirmDialog(this);
		confirmDialog.setPosition(
				stage.getWidth()+(stage.getWidth()-confirmDialog.getWidth())/2f,
				-stage.getHeight());
		return confirmDialog;
	}
	
	private Actor buildGameChooser() {
		gameChooser = new GameChooser(this);
		gameChooser.setPosition(
				stage.getWidth()+(stage.getWidth()-gameChooser.getWidth())/2f,
				stage.getHeight()*0.3f+(mainMenu.getHeight()-gameChooser.getHeight()));
		return gameChooser;
	}

	private Table buildOptionsWindowLayer() {
		winOptions = new OptionsWindow(this);
		winOptions.setPosition(
				-stage.getWidth()+(stage.getWidth() - winOptions.getWidth()) / 2f,
				stage.getHeight()*0.3f+(mainMenu.getHeight()-winOptions.getHeight()));
		return winOptions;
	}

	private void buildStage() {
		skin = Assets.instance.skin;
		titleStage.clear();
		titleStage.addActor(buildTitle());
		stage.clear();
		stage.addActor(buildMenu());
		stage.addActor(buildOptionsWindowLayer());
		stage.addActor(buildGameChooser());
		stage.addActor(buildConfirmDialog());
		stage.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				stage.unfocusAll();
				Gdx.input.setOnscreenKeyboardVisible(false);
			}
		});
	}

	public MenuScreen(AgeoGame game) {
		super(game);
	}

	@Override
	public void show() {
		Gdx.input.setCatchBackKey(true);
		Gdx.graphics.setContinuousRendering(false);
	    Gdx.graphics.requestRendering();
		stage = new Stage(new StretchViewport(Constants.LEVEL_SCREEN_WIDTH, Constants.LEVEL_SCREEN_HEIGHT));
		titleStage = new Stage(new StretchViewport(Constants.LEVEL_SCREEN_WIDTH, Constants.LEVEL_SCREEN_HEIGHT));
		InputMultiplexer inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(stage);
        inputMultiplexer.addProcessor(inputAdapter);
        Gdx.input.setInputProcessor(inputMultiplexer);
		GamePreferences.instance.load();
		buildStage();

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(Constants.PINK.r, Constants.PINK.g, Constants.PINK.b, Constants.PINK.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		titleStage.act(delta);
		titleStage.draw();
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
		titleStage.getViewport().update(width, height, true);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		stage.dispose();
		titleStage.dispose();
	}
}

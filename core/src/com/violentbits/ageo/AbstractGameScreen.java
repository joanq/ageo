package com.violentbits.ageo;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.violentbits.ageo.io.Assets;

/**
 * Base class for all screens in the game.
 */
public abstract class AbstractGameScreen implements Screen {
	/**
	 * A reference to the main Game object.
	 */
	protected AgeoGame game;
	/**
	 * Constructor.
	 * 
	 * @param game  A reference to the main Game object.
	 */
	public AbstractGameScreen(AgeoGame game) {
		this.game = game;
	}
	/**
	 * Called when the game is resumed. Reloads assets.
	 */
	@Override
	public void resume() {
		Assets.instance.init(new AssetManager());
	}
	/**
	 * Called when the game is disposed. Disposes assets.
	 */
	@Override
	public void dispose() {
		Assets.instance.dispose();
	}

}

package com.violentbits.ageo.instructions;

public class LeftWallInstruction extends MoveToWallInstruction implements RotableInstruction {
	public LeftWallInstruction() {
		dx=-1;
	}
}

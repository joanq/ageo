package com.violentbits.ageo.instructions;

public class UpWallInstruction extends MoveToWallInstruction implements RotableInstruction {
	public UpWallInstruction() {
		dy=1;
	}
}

package com.violentbits.ageo.instructions;

import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public class LeftInstruction extends MovementInstruction implements RotableInstruction {

	@Override
	public boolean runDefault(WorldController worldController, Bot bot) {
		return runDefault(worldController, bot, -1, 0);
	}
	
	@Override
	public boolean runInstantly(WorldController worldController, Bot bot) {
		return runInstantly(worldController, bot, -1, 0);
	}
}

package com.violentbits.ageo.instructions;

import com.violentbits.ageo.Constants;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public abstract class Instruction {
	private String name;

	protected Instruction() {
	}

	protected void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public boolean run(WorldController worldController, Bot bot) {
		if (Constants.IS_SOLVER_ACTIVE)
			return runInstantly(worldController, bot);
		else
			return runDefault(worldController, bot);
	}

	public abstract boolean runDefault(WorldController worldController, Bot bot);
	
	public boolean runInstantly(WorldController worldController, Bot bot) {
		return runDefault(worldController, bot);
	}

	public String getSoundName() {
		return null;
	}
}

package com.violentbits.ageo.instructions;

import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public class DeleteLeftInstruction extends DeleteInstruction {

	@Override
	public boolean runDefault(WorldController worldController, Bot bot) {
		runDefault(worldController, bot, -1);
		return true;
	}
	
	@Override
	public boolean runInstantly(WorldController worldController, Bot bot) {
		runInstantly(worldController, bot, -1);
		return true;
	}

}

package com.violentbits.ageo.instructions;

import com.violentbits.ageo.apparat.Program;
import com.violentbits.ageo.apparat.CellListener.CellEvent;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public class RepeatInstruction extends StateInstruction {
	private int initialValue;
	private int count;
	
	@Override
	protected void setName(String name) {
		String[] nameParts = name.split("#");
		super.setName(nameParts[0]);
		initialValue = count = Integer.parseInt(nameParts[1]);
	}

	@Override
	public String getName() {
		return super.getName()+"#"+count;
	}

	@Override
	public boolean end(WorldController worldController, Bot bot) {
		// Look for this in the program
		Program program = bot.getProgram();
		int counter = program.getIndex(program.getCounter()-1);
		while (program.getInstruction(counter)!=this
				&& counter != program.getCounter()) {
			counter = program.getIndex(counter-1);
		}
		if (program.getCounter()!=counter) {
			// Found it!
			if (count>1) {
				count--;
				program.setCounter(counter);
				program.pushPendingInstruction(this);
			} else {
				count = initialValue;
			}
			program.setInstruction(counter, this);
			program.sendEvent(counter, CellEvent.REPEAT);
		}
		return true;
	}
}

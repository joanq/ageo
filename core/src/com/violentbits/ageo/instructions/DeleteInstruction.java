package com.violentbits.ageo.instructions;

import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.violentbits.ageo.apparat.CellListener.CellEvent;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;
import com.violentbits.ageo.apparat.Program;

public abstract class DeleteInstruction extends Instruction {
	public void runDefault(WorldController worldController, final Bot bot, final int dif) {
		final int currentCounter = bot.getProgram().getCounter();
		Timer.instance().scheduleTask(new Task() {
			@Override
			public void run() {
				Program program = bot.getProgram();
				int pos = program.getIndex(currentCounter+dif);
				program.removeInstruction(pos);
				program.sendEvent(pos, CellEvent.DELETE);
			}
		}, worldController.actionTime*0.9f);
	}
	
	public void runInstantly(WorldController worldController, final Bot bot, final int dif) {
		final int currentCounter = bot.getProgram().getCounter();
		Program program = bot.getProgram();
		int pos = program.getIndex(currentCounter+dif);
		program.removeInstruction(pos);
		program.sendEvent(pos, CellEvent.DELETE);
	}

	@Override
	public String getSoundName() {
		return "delete.wav";
	}
}

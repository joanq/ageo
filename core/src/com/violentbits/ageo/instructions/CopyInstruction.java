package com.violentbits.ageo.instructions;

import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.violentbits.ageo.apparat.Program;
import com.violentbits.ageo.apparat.CellListener.CellEvent;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public class CopyInstruction extends Instruction {

	@Override
	public boolean runDefault(WorldController worldController, Bot bot) {
		boolean ok = false;
		final Program program = bot.getProgram();
		final int pos = program.getCounter()+1;
		if (program.getCells().size > program.getNInstructions()) {
			Timer.instance().scheduleTask(new Task() {
				@Override
				public void run() {
					Instruction toCopy = program.getInstruction(program.getIndex(pos));
					program.addInstruction(pos, InstructionFactory.instance.getInstruction(toCopy));
					program.sendEvent(pos, CellEvent.COPY);	
				}
			}, worldController.actionTime*0.9f);
			ok = true;
		}
		return ok;
	}
	
	@Override
	public boolean runInstantly(WorldController worldController, Bot bot) {
		boolean ok = false;
		final Program program = bot.getProgram();
		final int pos = program.getCounter()+1;
		if (program.getCells().size > program.getNInstructions()) {
			Instruction toCopy = program.getInstruction(program.getIndex(pos));
			program.addInstruction(pos, InstructionFactory.instance.getInstruction(toCopy));
			program.sendEvent(pos, CellEvent.COPY);
			ok = true;
		}
		return ok;
	}

}

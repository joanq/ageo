package com.violentbits.ageo.instructions;

import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public class WaitInstruction extends Instruction {

	@Override
	public boolean runDefault(WorldController worldController, Bot bot) {
		return true;
	}

}

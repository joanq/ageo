package com.violentbits.ageo.instructions;

import com.violentbits.ageo.apparat.Program;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public class EndInstruction extends Instruction {

	@Override
	public boolean runDefault(WorldController worldController, Bot bot) {
		Program program = bot.getProgram();
		if (program.hasPendingInstructions()) {
			program.popPendingInstruction().end(worldController, bot);
		}
		return true;
	}

}

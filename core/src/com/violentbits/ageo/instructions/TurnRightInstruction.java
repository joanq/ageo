package com.violentbits.ageo.instructions;

import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public class TurnRightInstruction extends TurnInstruction {

	@Override
	public boolean runDefault(WorldController worldController, Bot bot) {
		runDefault(worldController, bot, -90f);
		return true;
	}

	@Override
	public boolean runInstantly(WorldController worldController, Bot bot) {
		runInstantly(bot, -90f);
		return true;
	}
}

package com.violentbits.ageo.instructions;

import com.violentbits.ageo.apparat.Program;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public class JumpInstruction extends Instruction {

	@Override
	public boolean runDefault(WorldController worldController, Bot bot) {
		Program program = bot.getProgram();
		program.setCounter(program.getIndex(program.getCounter()+1));
		return true;
	}

}

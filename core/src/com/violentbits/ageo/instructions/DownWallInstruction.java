package com.violentbits.ageo.instructions;

public class DownWallInstruction extends MoveToWallInstruction implements RotableInstruction {
	public DownWallInstruction() {
		dy=-1;
	}
}

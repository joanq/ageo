package com.violentbits.ageo.instructions;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.MapController;
import com.violentbits.ageo.game.WorldController;

public abstract class MovementInstruction extends Instruction {
	private boolean check(WorldController worldController, Bot bot, int dx, int dy) {
		boolean ok = true;
		MapController mapController = worldController.getMapController();
		
		if (mapController.getProperty("kill", bot.getX(), bot.getY()).contains(getName().toLowerCase())) {
			ok = false;
			bot.setActive(false);
		} else if (mapController.getProperty("block", bot.getX(), bot.getY()).contains(getDirection(dx,dy))) {
			ok = false;
		} else {
			// Check collisions between bots
			Bot otherBot = worldController.getBotAtPosition(bot.getX()+dx, bot.getY()+dy);
			if (otherBot != null && otherBot != bot) {
				ok = false;
				// kill other bot?
				if (bot.killOnHit && bot.id!=otherBot.id) {
					otherBot.setActive(false);
				}
			}
		}
		return ok;
	}
	
	public boolean runDefault(WorldController worldController, Bot bot, int dx, int dy) {
		boolean ok = check(worldController, bot, dx, dy);
		if (ok) {
			bot.addAction(Actions.moveBy(dx, dy, worldController.actionTime*0.8f));
			worldController.getMapController().runTrigger(worldController, bot, bot.getX()+dx, bot.getY()+dy);
		}
		return ok;
	}
	
	public boolean runInstantly(WorldController worldController, Bot bot, int dx, int dy) {
		boolean ok = check(worldController, bot, dx, dy);
		if (ok) {
			bot.setPosition(bot.getX()+dx, bot.getY()+dy);
			worldController.getMapController().runTrigger(worldController, bot, bot.getX(), bot.getY());
		}
		return ok;
	}
	
	private static String getDirection(int dx, int dy) {
		String dir="";
		if (dx==0)
			dir=dy>0?"up":"down";
		else
			dir=dx>0?"right":"left";
		return dir;
	}
	
	@Override
	public String getSoundName() {
		return "move.ogg";
	}
}

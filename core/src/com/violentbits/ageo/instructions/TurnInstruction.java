package com.violentbits.ageo.instructions;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public abstract class TurnInstruction extends Instruction {
	private void normalizeRotation(Bot bot) {
		if (bot.getRotation()<-10)
			bot.setRotation(bot.getRotation()+360);
		else if (bot.getRotation()>370)
			bot.setRotation(bot.getRotation()-360);
	}
	
	public void runDefault(WorldController worldController, Bot bot, float angle) {
		normalizeRotation(bot);
		bot.addAction(Actions.rotateBy(angle, worldController.actionTime*0.8f));
	}

	public void runInstantly(Bot bot, float angle) {
		normalizeRotation(bot);
		bot.setRotation(bot.getRotation()+angle);
	}
}

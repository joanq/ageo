package com.violentbits.ageo.instructions;

import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public class SelfDestructInstruction extends Instruction {

	@Override
	public boolean runDefault(WorldController worldController, Bot bot) {
		boolean ok = false;
		if (bot.isActive()) {
			bot.setActive(false);
			ok = true;
		}
		return ok;
	}

}

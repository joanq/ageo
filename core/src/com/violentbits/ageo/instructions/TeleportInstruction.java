package com.violentbits.ageo.instructions;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.removeAction;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public class TeleportInstruction extends Instruction {
	private int x, y;

	@Override
	protected void setName(String name) {
		String[] nameParts = name.split("#");
		super.setName(nameParts[0]);
		x = Integer.parseInt(nameParts[1]);
		y = Integer.parseInt(nameParts[2]);
	}

	@Override
	public String getName() {
		return super.getName()+"#"+x+"#"+y;
	}
	
	@Override
	public boolean runDefault(WorldController worldController, Bot bot) {
		Action currentAction = bot.getActions().first();
		bot.addAction(sequence(
				fadeOut(worldController.actionTime*0.4f),
				removeAction(currentAction),
				moveTo(x, y),
				fadeIn(worldController.actionTime*0.4f)
		));
		return true;
	}

	@Override
	public boolean runInstantly(WorldController worldController, Bot bot) {
		bot.setPosition(x, y);
		return true;
	}
}

package com.violentbits.ageo.instructions;

import com.violentbits.ageo.apparat.CellListener.CellEvent;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;
import com.violentbits.ageo.apparat.Program;

public class SwapInstruction extends Instruction {
	@Override
	public boolean runDefault(WorldController worldController, Bot bot) {
		boolean ok=false;
		Program program = bot.getProgram();
		if (program.getNInstructions()>=3) {
			int counter = program.getCounter();
			int posLeft = program.getIndex(counter-1);
			int posRight = program.getIndex(counter+1);
			Instruction left = program.getInstruction(posLeft);
			Instruction right = program.setInstruction(posRight, left);
			program.setInstruction(posLeft, right);
			program.sendEvent(posLeft, CellEvent.SWAP);
			program.sendEvent(posRight, CellEvent.SWAP);
			ok = true;
		}
		return ok;
	}

	@Override
	public String getSoundName() {
		return "swap.wav";
	}
}

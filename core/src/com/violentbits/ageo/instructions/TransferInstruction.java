package com.violentbits.ageo.instructions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.violentbits.ageo.apparat.Program;
import com.violentbits.ageo.apparat.CellListener.CellEvent;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.Bots;
import com.violentbits.ageo.game.WorldController;
import com.violentbits.ageo.io.Assets;

public class TransferInstruction extends Instruction {

	@Override
	public boolean runDefault(WorldController worldController, Bot bot) {
		boolean ok=false;
		Bot facingBot = Bots.findFacingBot(worldController, bot);
		if (facingBot!=null) {
			int n = computeTransfer(worldController, bot, facingBot);
			transferDefault(worldController, bot, facingBot.getProgram(), n);
			ok = true;
		}
		return ok;
	}
	
	@Override
	public boolean runInstantly(WorldController worldController, Bot bot) {
		boolean ok=false;
		Bot facingBot = Bots.findFacingBot(worldController, bot);
		if (facingBot!=null) {
			int n = computeTransfer(worldController, bot, facingBot);
			transferInstantly(bot.getProgram(), facingBot.getProgram(), n);
			ok = true;
		}
		return ok;
	}
	
	private int computeTransfer(WorldController worldController, Bot bot, Bot other) {
		final Program program = bot.getProgram();
		final Program otherProgram = other.getProgram();
		
		int otherInitialCounter = otherProgram.getCounter();
		boolean transferEnded = false;
		int nTransfered=0;
		int dCounter = 1;
		int dOtherCounter = 0;
		int counter, otherCounter;
		Instruction instruction;
		
		while (!transferEnded) {
			counter = program.getIndex(program.getCounter()+dCounter);
			instruction = program.getInstruction(counter);
			otherCounter = (otherProgram.getCounter()+dOtherCounter)%otherProgram.getCells().size;
			if (counter==program.getCounter() ||
					instruction instanceof EndInstruction ||
					otherProgram.getCells().size <= otherProgram.getNInstructions()+nTransfered) {
				transferEnded = true;
			} else {
				Gdx.app.debug("TransferInstruction", counter+"("+instruction.getName()+") to "+otherCounter);
				
				program.sendEvent(counter, CellEvent.TRANSFER);
				otherProgram.sendEvent(otherCounter, CellEvent.TRANSFER);
				nTransfered++;
				dOtherCounter++;
				dCounter++;
			}
		}
		otherProgram.setCounter(otherInitialCounter);
		return nTransfered;
	}

	private void transferDefault(WorldController worldController, final Bot bot, final Program otherProgram, final int n) {
		if (n>0) {
			Timer.instance().scheduleTask(new Task() {
				@Override
				public void run() {
					transferInstantly(bot.getProgram(), otherProgram, n);
				}
			}, worldController.actionTime*0.5f);
		}
		bot.addEffect(Assets.instance.effect);
	}
	
	private void transferInstantly(final Program program, final Program otherProgram, final int n) {
		int counter;
		int otherCounter;
		for (int i=0; i<n; i++) {
			otherCounter = otherProgram.getIndex(otherProgram.getCounter());
			counter = program.getIndex(program.getCounter());
			otherProgram.addInstruction(otherCounter, program.getInstruction(counter));
			program.removeInstruction(counter);
		}
	}
}

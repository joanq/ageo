package com.violentbits.ageo.apparat;

import com.violentbits.ageo.instructions.Instruction;

public class Cell extends AbstractCell {
	private final Program program;
	private final int pos;
	
	public Cell(Program program, int pos) {
		this.program = program;
		this.pos = pos;
	}
	
	public Program getProgram() {
		return program;
	}
	
	public int getPos() {
		return pos;
	}
	
    protected Instruction setInstruction(Instruction instruction) {
    	Instruction oldInstruction = this.instruction;
    	this.instruction = instruction;
    	notifyListeners(oldInstruction);
    	return oldInstruction;
    }

    @Override
    public boolean put(Instruction instruction) {
    	return program.addInstruction(pos, instruction);
    }

    @Override
    public Instruction remove() {
    	return program.removeInstruction(pos);
    }
}

package com.violentbits.ageo.apparat;

import com.violentbits.ageo.instructions.Instruction;

/**
 * Interface used to be notified of changes in a memory cell.
 */
public interface CellListener {
	/**
	 * Called every time a listened cell changes its content. Use AbstractCell.addListener()
	 * to register the listener.
	 * 
	 * @param cell  The cell that triggers the event.
	 * @param oldValue  The old instruction stored in the cell.
	 * @param newValue  The new instruction stored in the cell.
	 */
	public void changed(AbstractCell cell, Instruction oldValue, Instruction newValue);
	/**
	 * Called when a instruction is executed. It is used to activate the
	 * associated animation in the cell.
	 * 
	 * @param event  The event (type of instruction) executed.
	 * @param cell  The cell where the executed instruction is.
	 * @param eventInfo  Additional information used by this event.
	 */
	public void event(CellEvent event, AbstractCell cell, Object eventInfo);
	/**
	 * Possible cell events. A list of instruction types that cause an animation
	 * to be activated on the cell.
	 */
	public enum CellEvent {
		DELETE, SWAP, COPY, TRANSFER, REPEAT, ROTATE, ROTATE_INSTANTLY;
	}
}

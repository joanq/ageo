package com.violentbits.ageo.apparat;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.SnapshotArray;
import com.violentbits.ageo.Constants;
import com.violentbits.ageo.SoundController;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;
import com.violentbits.ageo.instructions.Instruction;
import com.violentbits.ageo.instructions.InstructionFactory;
import com.violentbits.ageo.io.Assets;

public class InstructionsTable extends Table {
	protected final WorldController worldController;
	protected final Table buttonRow;
	protected final ScrollPane scrollPane;
	private DragAndDrop dragAndDrop;
	private Bot bot;
	private TextButton hideButton = new TextButton(">>", Assets.instance.skin);

	private ChangeListener cellClicked = new ChangeListener() {
		@Override
		public void changed(ChangeEvent event, Actor actor) {
			CellActor cellActor = (CellActor) event.getListenerActor();
			if (!worldController.isRunning()) {
				Instruction instruction = cellActor.getCell().getInstruction();
				if (bot.getProgram().addInstruction(InstructionFactory.instance.getInstruction(instruction)))
					SoundController.instance.play(Assets.instance.getSound("select.wav"));
				else
					SoundController.instance.play(Assets.instance.getSound("no.ogg"));
			}
		}
	};

	private void setCells(Array<? extends AbstractCell> cells, boolean editable) {
		buttonRow.clear();
		for (AbstractCell cell : cells) {
			CellActor cellActor = new CellActor(worldController, cell);
			buttonRow.add(cellActor).size(Constants.INSTRUCTION_ICON_SIZE).row();

			if (editable) {
				dragAndDrop.addSource(new CellSource(worldController, cellActor));
				dragAndDrop.addTarget(new CellTarget(cellActor));
			}
		}
		buttonRow.pack();
	}

	public void setBot(Bot bot) {
		if (this.bot != bot) {
			this.bot = bot;
			setCells(bot.getInstructionSet().getCells(), bot.editable);
			createClickListeners();
		}
	}

	public InstructionsTable(WorldController worldController, DragAndDrop dragAndDrop) {
		super(Assets.instance.skin);
		this.worldController = worldController;
		this.dragAndDrop = dragAndDrop;

		hideButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if ((hideButton.getText() + "").equals(">>")) {
					hideButton.setText("<<");
					addAction(Actions.moveBy(Constants.INSTRUCTION_ICON_SIZE, 0, 0.4f));
				} else {
					hideButton.setText(">>");
					addAction(Actions.moveBy(-Constants.INSTRUCTION_ICON_SIZE, 0, 0.4f));
				}
				setVisible(true);
			}
		});
		add(hideButton).height(Constants.INSTRUCTION_ICON_SIZE);

		dragAndDrop.setDragActorPosition(-Constants.INSTRUCTION_ICON_SIZE / 2, Constants.INSTRUCTION_ICON_SIZE / 2);

		buttonRow = new Table();
		buttonRow.defaults().spaceTop(Constants.INSTRUCTIONS_SEPARATION);
		scrollPane = new ScrollPane(buttonRow);
		add(scrollPane).fillY();
		pack();
		setFillParent(true);
		setVisible(true);
		right().top();
	}

	private void createClickListeners() {
		for (Actor actor : buttonRow.getChildren()) {
			if (actor instanceof CellActor) {
				actor.addListener(cellClicked);
			}
		}
	}

	public CellActor getCellActor(int index) {
		return (CellActor)buttonRow.getChildren().get(index);
	}
	
	public SnapshotArray<Actor> getCellActors() {
		return buttonRow.getChildren();
	}
	
	public void setDisabled(boolean isDisabled) {
		for (Actor actor : buttonRow.getChildren()) {
			if (actor instanceof CellActor) {
				CellActor cellActor = (CellActor) actor;
				cellActor.setDisabled(isDisabled);
				cellActor.setTouchable(isDisabled?Touchable.disabled:Touchable.enabled);
			}
		}
	}
}

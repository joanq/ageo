package com.violentbits.ageo.apparat;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Payload;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Source;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Target;
import com.violentbits.ageo.Constants;
import com.violentbits.ageo.game.WorldController;
import com.violentbits.ageo.instructions.Instruction;
import com.violentbits.ageo.instructions.InstructionFactory;
import com.violentbits.ageo.io.Assets;

public class CellSource extends Source {
	private AbstractCell sourceCell;
	private WorldController worldController;

	public CellSource(WorldController worldController, CellActor actor) {
		super(actor);
		this.sourceCell = actor.getCell();
		this.worldController = worldController;
	}

	@Override
	public Payload dragStart(InputEvent event, float x, float y, int pointer) {
		Payload payload = null;
		if (!worldController.isRunning() && !sourceCell.isEmpty() && !((Button) this.getActor()).isDisabled()) {
			payload = new Payload();
			Instruction payloadInstruction = sourceCell.remove();
			payload.setObject(payloadInstruction);

			TextureRegion icon = Assets.instance.get(payloadInstruction.getName());

			Actor dragActor = new Image(icon);
			dragActor.setColor(1, 0, 0, 0.5f);
			dragActor.setSize(Constants.INSTRUCTION_ICON_SIZE, Constants.INSTRUCTION_ICON_SIZE);
			payload.setDragActor(dragActor);

			Actor validDragActor = new Image(icon);
			validDragActor.setColor(0, 1, 0, 0.8f);
			validDragActor.setSize(Constants.INSTRUCTION_ICON_SIZE, Constants.INSTRUCTION_ICON_SIZE);
			payload.setValidDragActor(validDragActor);

			Actor invalidDragActor = new Image(icon);
			invalidDragActor.setColor(1, 0, 0, 0.5f);
			invalidDragActor.setSize(Constants.INSTRUCTION_ICON_SIZE, Constants.INSTRUCTION_ICON_SIZE);
			payload.setInvalidDragActor(invalidDragActor);
		}
		return payload;
	}

	@Override
	public void dragStop(InputEvent event, float x, float y, int pointer, Payload payload, Target target) {
		if (payload != null) {
			Instruction payloadInstruction = (Instruction) payload.getObject();
			if (target != null && target.getActor().isTouchable()) {
				// the payload was dropped over a valid target
				AbstractCell targetCell = ((CellActor) target.getActor()).getCell();
				targetCell.put(InstructionFactory.instance.getInstruction(payloadInstruction));
			}
			// else delete instruction
		}
	}
}

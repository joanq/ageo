package com.violentbits.ageo.apparat;

import java.util.Iterator;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Queue;
import com.violentbits.ageo.apparat.CellListener.CellEvent;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;
import com.violentbits.ageo.instructions.Instruction;
import com.violentbits.ageo.instructions.InstructionFactory;
import com.violentbits.ageo.instructions.StateInstruction;

/**
 * A program that can be executed by a bot. The program
 * consists in a cells array, and the instructions stored
 * in them.
 */
public class Program {
	@SuppressWarnings("unused")
	private static final String TAG = Program.class.getName();
	private Array<Cell> cells = new Array<Cell>();
	private Array<Instruction> backup = new Array<Instruction>();
	private Queue<StateInstruction> pendingInstructions = new Queue<StateInstruction>();
	
	/**
	 * Index to the next instruction to be executed.
	 */
	private int counter = 0;
	/**
	 * Number of instructions that this program has.
	 */
	private int nInstructions = 0;
	
	private Array<ProgramListener> listeners = new Array<ProgramListener>();
	
	/**
	 * Add a listener that will be notified when an instruction is executed.
	 * 
	 * @param listener  The ProgramListener to add.
	 */
	public void addListener(ProgramListener listener) {
		listeners.add(listener);
	}
	/**
	 * Remove a listener from the notification list of this program.
	 * 
	 * @param listener  The ProgramListener that should not receive more notifications.
	 * @return
	 */
	public boolean removeListener(ProgramListener listener) {
		return listeners.removeValue(listener, true);
	}
	/**
	 * Notify all listener about the execution of one of this program instructions.
	 * 
	 * @param bot  The bot associated with this program.
	 * @param instruction  The executed instruction.
	 * @param index  The position of the executed instruction in the cell's array.
	 */
	protected void notifyListeners(Bot bot, Instruction instruction, int index) {
		for (ProgramListener listener : listeners) {
			listener.executed(bot, instruction, index);
		}
	}
	/**
	 * Keep a copy of the current instructions of this program.
	 */
	public void makeBackup() {
		backup.clear();
		Iterator<Cell> it = cells.iterator();
		for (int i=0; i<nInstructions; i++) {
			backup.add(InstructionFactory.instance.getInstruction(it.next().getInstruction()));
		}
	}
	/**
	 * Restore a previously stored backup of the instructions in this program.
	 */
	public void restoreBackup() {
		Iterator<Cell> it = cells.iterator();
		for (Instruction ins : backup) {
			it.next().setInstruction(InstructionFactory.instance.getInstruction(ins));
		}
		for (int i=backup.size; i<cells.size; i++) {
			it.next().setInstruction(null);
		}
		nInstructions = backup.size;
		counter=0;
		pendingInstructions.clear();
	}
	/**
	 * Get the program counter, that is, the position of the next instruction
	 * to be executed.
	 * 
	 * @return  the program counter.
	 */
	public int getCounter() {
		return counter;
	}
	/**
	 * Get the number of instructions this program has.
	 * 
	 * @return
	 */
	public int getNInstructions() {
		return nInstructions;
	}
	/**
	 * Add an instruction to this program at the specified position. Move all
	 * instructions from this position one position forward. If the position
	 * is located after all other instructions, it will be modified in order to
	 * keep all instructions together.
	 * 
	 * @param pos  The index that the new instruction will have.
	 * @param instruction  The instruction to add.
	 * @return  true if the instruction was successfully added, false if there
	 * isn't space enough in this program.
	 */
	public boolean addInstruction(int pos, Instruction instruction) {
		int n;
		boolean ok = false;
		if (nInstructions < cells.size) {
			// keep instructions together
			if (pos>nInstructions)
				pos = nInstructions;
			// move the following instructions to the right
			for (n=nInstructions; n>=pos+1; n--) {
				cells.get(n).setInstruction(cells.get(n-1).getInstruction());
			}
			cells.get(pos).setInstruction(instruction);
			nInstructions++;
			if (counter>=pos)
				counter = getIndex(counter+1);
			ok = true;
		}
		return ok;
	}
	
	public boolean addInstruction(Instruction instruction) {
		return addInstruction(nInstructions, instruction);
	}
	/**
	 * Remove the instruction that occupies the specified position. Move next
	 * instructions to the left to keep them together.
	 * 
	 * @param pos  The index of the instruction to remove.
	 * @return  The removed instruction, or null if there wasn't an instruction
	 * in this position.
	 */
	public Instruction removeInstruction(int pos) {
		int n;
		Instruction removed = null;
		if (pos<nInstructions) {
			removed = cells.get(pos).getInstruction();
			nInstructions--;
			for (n=pos; n<nInstructions; n++) {
				cells.get(n).setInstruction(cells.get(n+1).getInstruction());
			}
			cells.get(nInstructions).setInstruction(null);
			if (counter>pos)
				counter = getIndex(counter-1);
		}
		return removed;
	}
	/**
	 * Put an instruction in the place of another one.
	 * 
	 * @param pos  The index of the instruction.
	 * @param instruction  The instruction to add.
	 * @return  The removed instruction.
	 */
	public Instruction setInstruction(int pos, Instruction instruction) {
		Instruction oldInstruction = null;
		if (!cells.get(pos).isEmpty()) {
			oldInstruction = cells.get(pos).setInstruction(instruction);
		}
		return oldInstruction;
	}
	/**
	 * Return the instruction that occupy the specified position.
	 * 
	 * @param pos  The index to get.
	 * @return  The instruction in this position.
	 */
	public Instruction getInstruction(int pos) {
		if (pos<0 || pos>=nInstructions)
			throw new IllegalArgumentException(pos+" is out of range.");
		return cells.get(pos).getInstruction();
	}
	
	public void clear() {
		for (int pos=nInstructions-1; pos>=0; pos--) {
			cells.get(pos).setInstruction(null);
		}
		nInstructions=0;
	}
	/**
	 * Send an event notification to all this program listeners.
	 * 
	 * @param pos  The position of the affected instruction.
	 * @param event  The event type.
	 */
	public void sendEvent(int pos, CellEvent event) {
		cells.get(pos).notifyEvent(event);
	}
	/**
	 * Send an event notification to all this program listeners.
	 * 
	 * @param pos  The position of the affected instruction.
	 * @param event  The event type.
	 * @param eventInfo  Additional information needed for this event.
	 */
	public void sendEvent(int pos, CellEvent event, Object eventInfo) {
		cells.get(pos).notifyEvent(event, eventInfo);
	}
	/**
	 * Normalize an index using modulus, so it is between 0 and this
	 * program's number of instructions
	 * @param pos
	 * @return
	 */
	public int getIndex(int pos) {
		if (nInstructions>0) {
			pos = pos % nInstructions;
			if (pos<0)
				pos=pos+nInstructions;
		} else {
			pos = 0;
		}
		return pos;
	}
	/**
	 * Set a new position for this program's counter.
	 * 
	 * @param counter  The new program counter.
	 */
	public void setCounter(int counter) {
		if (counter<0 || counter>=cells.size)
			throw new IllegalArgumentException("Program counter not in range: "+counter);
		this.counter = counter;
	}
	/**
	 * Constructor that receives the number of cells.
	 * 
	 * @param size
	 */
	public Program(int size) {
        cells = new Array<Cell>(size);
        for (int i=0; i<size; i++) {
        	cells.add(new Cell(this, i));
        }
    }
	/**
	 * Constructor that receives the number of cells and the
	 * instructions.
	 * 
	 * @param size
	 * @param instructions
	 */
	public Program(int size, Instruction... instructions) {
		this(size);
		for (int i=0; i<instructions.length; i++) {
			cells.get(i).setInstruction(instructions[i]);
		}
		nInstructions = instructions.length;
		makeBackup();
	}
	
	public Array<Cell> getCells() {
        return cells;
    }
	/**
	 * Executes the next instruction of this program. Usually this
	 * should be called from the bot that have this program.
	 * 
	 * @param bot  The bot that is running the program.
	 */
	public Instruction step(WorldController worldController, Bot bot) {
		Instruction instruction = null;
		if (!cells.get(0).isEmpty()) {
			instruction = cells.get(counter).getInstruction();
			if (instruction==null) {
				counter = 0;
				instruction = cells.get(0).getInstruction();
			}
			int instructionPos = counter; // in case the instruction change the counter
			instruction.run(worldController, bot);
			notifyListeners(bot, instruction, instructionPos);
			counter = nInstructions>0?(counter+1)%nInstructions:0;
		}
		return instruction;
	}
	
	public boolean hasPendingInstructions() {
		return pendingInstructions.size!=0;
	}
	
	public StateInstruction popPendingInstruction() {
		//Gdx.app.debug(TAG, "Pop "+pendingInstructions.toString());
		return pendingInstructions.removeLast();
	}
	
	public void pushPendingInstruction(StateInstruction instruction) {
		pendingInstructions.addLast(instruction);
		//Gdx.app.debug(TAG, "Push "+pendingInstructions.toString());
	}
	
	public StateInstruction peekPendingInstruction() {
		return pendingInstructions.last();
	}

	public boolean hasMemory() {
		return pendingInstructions.size!=0;
	}
}

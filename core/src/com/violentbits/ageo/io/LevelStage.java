package com.violentbits.ageo.io;

import com.badlogic.gdx.utils.Array;

public class LevelStage {
	public String name;
	public String index;
	public Array<LevelInfo> levels;
}

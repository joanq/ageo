package com.violentbits.ageo.io;

public class LevelInfo {
	public String name;
	public String map;
	public String bots;
	public int[] pos;
	
	public String[] messages;
	public boolean[] left;
	public String[] soundNames;
	public String[] tutorial;
	
	public int minSteps;
	public int minIns;
	
	public LevelInfo() {}
	
	public LevelInfo(String name, String map, String bots) {
		this.name = name;
		this.map = map;
		this.bots = bots;
	}
}

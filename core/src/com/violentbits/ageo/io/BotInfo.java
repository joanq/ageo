package com.violentbits.ageo.io;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.violentbits.ageo.Constants;
import com.violentbits.ageo.apparat.AbstractCell;
import com.violentbits.ageo.apparat.InstructionSet;
import com.violentbits.ageo.apparat.Program;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.DefaultBot;
import com.violentbits.ageo.instructions.Instruction;
import com.violentbits.ageo.instructions.InstructionFactory;
import com.violentbits.ageo.solver.InstantlyBot;

public class BotInfo {
	private Array<String> instructionSet = new Array<String>();
	private Array<String> program = new Array<String>();
	private int programSize;
	private int x;
	private int y;
	private Color flagColor;
	private String texture;
	private boolean editable=true;
	private boolean killOnHit;
	private int id;
	private int speed=1;
	private int rotation=0;
	private boolean active=true;
	
	public BotInfo() {}
	
	public BotInfo(Bot bot) {
		for (AbstractCell cell : bot.getInstructionSet().getCells()) {
			instructionSet.add(cell.getInstruction().getName());
		}
		for (AbstractCell cell : bot.getProgram().getCells()) {
			if (cell.getInstruction() != null) {
				program.add(cell.getInstruction().getName());
			}
		}
		programSize = bot.getProgram().getCells().size;
		x = MathUtils.round(bot.getX());
		y = MathUtils.round(bot.getY());
		flagColor = bot.getActivatedColor();
		texture = bot.getTextureName();
		editable = bot.editable;
		killOnHit = bot.killOnHit;
		id = bot.id;
		speed = bot.getSpeed();
		rotation = MathUtils.round(bot.getRotation());
		active = bot.isActive();
	}
	
	public Bot createBot() {
		Bot bot;
		if (Constants.IS_SOLVER_ACTIVE) {
			bot = new InstantlyBot(texture, flagColor);
		} else {
			bot = new DefaultBot(texture, flagColor);
		}
		bot.setInitialPosition(x,y,rotation,active);
		Instruction[] p = new Instruction[program.size];
		for (int i=0; i<program.size; i++) {
			p[i] = InstructionFactory.instance.getInstruction(program.get(i));
		}
		bot.setProgram(new Program(programSize, p));
		p = new Instruction[instructionSet.size];
		for (int i=0; i<instructionSet.size; i++) {
			p[i] = InstructionFactory.instance.getInstruction(instructionSet.get(i));
		}
			
		bot.setInstructionSet(new InstructionSet(p));
		bot.editable = editable;
		bot.killOnHit = killOnHit;
		bot.id = id;
		bot.setSpeed(speed);
		
		return bot;
	}
}

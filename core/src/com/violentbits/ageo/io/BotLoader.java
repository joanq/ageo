package com.violentbits.ageo.io;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SynchronousAssetLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter;
import com.violentbits.ageo.game.Bot;

public class BotLoader extends SynchronousAssetLoader<BotArray, BotLoader.BotLoaderParameter> {
	public BotLoader(FileHandleResolver resolver) {
		super(resolver);
	}
	
	public void writeBots(FileHandle file, Array<Bot> bots) {
		Array<BotInfo> botsInfo = new Array<BotInfo>();
		for (Bot bot : bots)
			botsInfo.add(new BotInfo(bot));
		Json json = new Json(JsonWriter.OutputType.json);
		json.addClassTag("bot", BotInfo.class);
		file.writeString(json.prettyPrint(botsInfo), false);
	}
	
	public Array<Bot> readBots(FileHandle file) {
		Array<Bot> bots = new Array<Bot>();
		Json json = new Json();
		json.addClassTag("bot", BotInfo.class);
		@SuppressWarnings("unchecked")
		Array<BotInfo> botsInfo = json.fromJson(Array.class, file);
		for (BotInfo botInfo : botsInfo)
			bots.add(botInfo.createBot());
		return bots;
	}
	
	public void writeBot(FileHandle file, Bot bot) {
		BotInfo botInfo = new BotInfo(bot);
		Json json = new Json(JsonWriter.OutputType.json);
		file.writeString(json.prettyPrint(botInfo), false);
	}
	
	public Bot readBot(FileHandle file) {
		Json json = new Json();
		BotInfo botInfo = json.fromJson(BotInfo.class, file);
		return botInfo.createBot();
	}
	
	public static class BotLoaderParameter extends AssetLoaderParameters<BotArray> {
	}

	@Override
	public BotArray load(AssetManager assetManager, String fileName, FileHandle file, BotLoaderParameter parameter) {
		BotArray botArray = new BotArray();
		botArray.bots = readBots(file);
		return botArray;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, BotLoaderParameter parameter) {
		return null;
	}
}

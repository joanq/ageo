package com.violentbits.ageo.victory;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.violentbits.ageo.game.WorldController;

public interface VictoryCondition {
	public boolean checkVictory(WorldController worldController);
	public void help(WorldController worldController, Stage worldStage, Stage hudStage);
}

package com.violentbits.ageo;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.MathUtils;
import com.violentbits.ageo.game.WorldController;

public class DirectionsHelper {
	public static final String[] directions={"Right","Up","Left","Down"};

	private DirectionsHelper() {}
	
	public static String rotate(String direction, int rotation) {
		String newDirection=direction;
		int jump = (rotation/90+4)%4;
		int pos=-1;
		for (int i=0; i<directions.length; i++) {
			if (directions[i].equals(direction)) {
				pos=i;
				break;
			}
		}
		if (pos>=0) {
			newDirection=directions[(pos+jump)%4];
		}
		return newDirection;
	}
	
	public static float getCurrentRotation(WorldController worldController) {
		Camera cam = worldController.getBots().first().getStage().getCamera();
		return MathUtils.radDeg*MathUtils.atan2(cam.up.y, cam.up.x)-90f;
	}
}

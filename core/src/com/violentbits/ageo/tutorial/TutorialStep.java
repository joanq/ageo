package com.violentbits.ageo.tutorial;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;

/**
 * A single step in a tutorial.
 */
public class TutorialStep {
	protected Actor target;
	private Arrow arrow;
	private TutorialEvent endEvent;
	
	/**
	 * Constructor.
	 * 
	 * @param target  The actor to point to with an arrow, can be null.
	 * @param endEvent  The event that marks the end of this step.
	 */
	public TutorialStep(Actor target, TutorialEvent endEvent) {
		this.target = target;
		this.endEvent = endEvent;
	}
	/**
	 * Called when this step is activated in the tutorial sequence.
	 * This method creates an arrow pointing to its target, and makes
	 * sure the target is touchable.
	 */
	public void start() {
		if (target!=null) {
			arrow = new Arrow(target);
			if (target instanceof Button) {
				Button button = (Button) target;
				button.setDisabled(false);
			}
			target.setTouchable(Touchable.enabled);
		}
	}
	/**
	 * Called when this step is over. This remove the arrow and
	 * disables the target.
	 */
	public void end() {
		if (arrow!=null) {
			arrow.remove();
		}
		if (target != null) {
			if (target instanceof Button) {
				Button button = (Button) target;
				button.setDisabled(true);
			}
			target.setTouchable(Touchable.disabled);
		}
	}
	/**
	 * Get the event that ends this step.
	 * 
	 * @return  The event that indicates the end of this step.
	 */
	public TutorialEvent getEndEvent() {
		return endEvent;
	}
	/**
	 * Get this step target.
	 * 
	 * @return  This step target.
	 */
	public Actor getTarget() {
		return target;
	}
	/**
	 * Possible events for each tutorial step.
	 */
	public enum TutorialEvent {
		ON_WIN, ON_INSTRUCTION_ADDED, ON_INSTRUCTION_REMOVED, ON_PLAY, ON_STOP, ON_NSTEPS, ON_BOT_SELECTED, ON_CLICK
	}
}

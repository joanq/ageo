package com.violentbits.ageo.tutorial;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.violentbits.ageo.game.HudRenderer;
import com.violentbits.ageo.io.Assets;
import com.violentbits.ageo.scene.Indications;
import com.violentbits.ageo.scene.Indications.Dir;

public class HelpInfoTutorialStep extends TutorialStep {
	private HudRenderer renderer;
	private Indications indications;
	
	public HelpInfoTutorialStep(HudRenderer renderer) {
		super(null, TutorialEvent.ON_CLICK);
		this.renderer = renderer;
	}
	
	@Override
	public void start() {
		Stage stage = renderer.getHudStage();
		indications = new Indications(stage, Assets.instance.skin);
		indications.addLabelAndLine(renderer.getBtnStop(), "Stop", Indications.Dir.UP, stage.getHeight()/5f);
		indications.addLabelAndLine(renderer.getBtnPause(), "Pause", Dir.UP, stage.getHeight()/5f);
		indications.addLabelAndLine(renderer.getBtnPlay(), "Play",Dir.UP, stage.getHeight()/5f);
		indications.addLabelAndLine(renderer.getBtnFastForward(), "Fast forward", Dir.UP, stage.getHeight()/5f);
		indications.addLabelAndLine(renderer.getBtnStep(), "Step", Dir.UP, stage.getHeight()/5f-30f);
		indications.addLabelAndLine(renderer.getBtnHelp(), "Show goal", Dir.UP, stage.getHeight()/5f);
		indications.addLabelAndLine(renderer.getBtnClear(), "Clear program", Dir.UP, stage.getHeight()/5f-30f);
		indications.addLabelAndLine(renderer.getInstructionsTable().getChildren().first(), "Available instructions", Dir.RIGHT, stage.getWidth()/5f);
		indications.addLabelAndLine(renderer.getProgramTable().getChildren().first(), "Selected bot program", Dir.DOWN, stage.getHeight()/5f);
	}
		
	@Override
	public void end() {
		indications.removeAll();
	}
}

package com.violentbits.ageo.tutorial;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.violentbits.ageo.apparat.CellActor;
import com.violentbits.ageo.io.Assets;

public class InstructionRemovedTutorialStep extends TutorialStep {
	private Image image;

	public InstructionRemovedTutorialStep(Actor target) {
		super(target, TutorialEvent.ON_INSTRUCTION_REMOVED);
	}
	
	@Override
	public void start() {
		super.start();
		if (getTarget()!=null) {
			deleteMovement();
		}
	}

	@Override
	public void end() {
		super.end();
		if (image!=null) {
			image.remove();
			image=null;
		}
	}
	
	private void deleteMovement() {
		if (getTarget() instanceof CellActor) {
			CellActor cellActor = (CellActor) getTarget();
			TextureRegion icon = Assets.instance.get(cellActor.getCell().getInstruction().getName());
			image = new Image(icon);
			image.setPosition(cellActor.getX(), cellActor.getY());
			image.setSize(cellActor.getWidth(), cellActor.getHeight());
			cellActor.getStage().addActor(image);
			image.addAction(Actions.forever(Actions.sequence(
				Actions.parallel(Actions.moveBy(30, 30, 1f), Actions.alpha(0f, 1f)),
				Actions.moveTo(cellActor.getX(), cellActor.getY()),
				Actions.alpha(1f)
			)));
			image.setTouchable(Touchable.disabled);
		}
	}
}

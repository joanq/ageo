package com.violentbits.ageo.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.violentbits.ageo.apparat.InstructionSet;
import com.violentbits.ageo.apparat.Program;
import com.violentbits.ageo.instructions.Instruction;
import com.violentbits.ageo.io.Assets;
import com.violentbits.ageo.scene.ParticleEffectActor;

/**
 * Base class for all bots in the game. A bot is a game object
 * that is programmable and can execute actions.
 */
public abstract class Bot extends Image {
	@SuppressWarnings("unused")
	private static final String TAG = Bot.class.getName();
	/**
	 * Position at the start of the level.
	 */
	protected int initialX, initialY;
	/**
	 * Rotation at the beginning.
	 */
	protected int initialRotation;
	/**
	 * If this bot is active or no at the beginning.
	 */
	protected boolean initialActivation;
	/**
	 * The set of instructions supported by this bot.
	 */
	protected InstructionSet instructionSet;
	/**
	 * The program that this bot is running.
	 */
	protected Program program;
	/**
	 * Color to tint this bot's windows.
	 */
	private Color activatedColor;
	private Color deactivatedColor;
	/**
	 * Name of the texture used to draw this bot.
	 * Used only to save a bot in json format.
	 */
	private String textureName;
	/**
	 * If this bot is editable by the player or not.
	 */
	public boolean editable;
	/**
	 * If this bot kills (deactivates) another bot when hitting it.
	 */
	public boolean killOnHit;
	/**
	 * Identification number used in victory conditions.
	 */
	public int id;
	/**
	 * If this bot is active or not.
	 */
	private boolean active=true;
	/**
	 * Number of instructions that this bot can execute in a single turn.
	 */
	private int speed=1;
	/**
	 * A reference to the world controller.
	 */
	protected WorldController worldController;
	
	/**
	 * Constructor.
	 * 
	 * @param textureName  The name of the texture used to draw this bot.
	 * @param flagColor  The color used to tint this bot's windows.
	 */
	public Bot(String textureName, Color flagColor) {
		super(Assets.instance.get(textureName));
		this.activatedColor = flagColor;
		this.textureName = textureName;
		setSize(1, 1);
		setOrigin(getWidth()/2, getHeight()/2);
		this.setColor(flagColor);
		deactivatedColor = flagColor.cpy().mul(0.4f, 0.4f, 0.4f, 1f);
	}
	/**
	 * Get the color for this bot when it is activated.
	 * 
	 * @return the color used to tint this bot when it is activated.
	 */
	public Color getActivatedColor() {
		return activatedColor;
	}
	/**
	 * Get the color for this bot when it is deactivated.
	 * 
	 * @return the color used to tint this bot when it is deactivted.
	 */
	public Color getDeactivatedColor() {
		return deactivatedColor;
	}
	/**
	 * Get the name of the texture used to draw this bot.
	 * 
	 * @return the name of the texture
	 */
	public String getTextureName() {
		return textureName;
	}
	/**
	 * Assign a program to be executed by the bot.
	 * 
	 * @param program  The program to be executed.
	 */
	public void setProgram(Program program) {
		this.program = program;
	}
	/**
	 * Get the program that this bot is running.
	 * 
	 * @return the program for this bot.
	 */
	public Program getProgram() {
		return program;
	}
	/**
	 * Assigns the initial position for this bot.
	 * 
	 * @param x  x coordinate
	 * @param y  y coordinate
	 * @param rotation  the initial rotation
	 * @param active  if this bot is active or not
	 */
	public void setInitialPosition(int x, int y, int rotation, boolean active) {
		initialX = x;
		initialY = y;
		initialRotation = rotation;
		initialActivation = active;
		setPosition(x, y);
		setRotation(rotation);
	}
	/**
	 * Returns this bot and its program to the initial state.
	 * 
	 * @param worldController  A reference to the world controller.
	 */
	public void reset(WorldController worldController) {
		this.worldController = worldController;
		program.restoreBackup();
		setActive(initialActivation);
	}
	/**
	 * Assigns the set of instructions supported by this bot.
	 * 
	 * @param instructionSet  The set of instructions.
	 */
	public void setInstructionSet(InstructionSet instructionSet) {
		this.instructionSet = instructionSet;
	}
	/**
	 * Get the instruction set that this bot can run.
	 * 
	 * @return  the instruction set.
	 */
	public InstructionSet getInstructionSet() {
		return instructionSet;
	}
	/**
	 * Return whether this bot is active or not.
	 * 
	 * @return true if is active, false otherwise.
	 */
	public boolean isActive() {
		return active;
	}
	/**
	 * Activate or deactivate this bot.
	 * 
	 * @param active  true if the bot should be activated,
	 * false if it should be deactivated.
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	/**
	 * Executes the next instruction of this bot's program.
	 * 
	 * @return the executed instruction, or null if the bot is not active
	 * or has no program.
	 */
	public Instruction action() {
		Instruction instruction = null;
		if (active && program != null) {
			instruction = program.step(worldController, this);
		}
		return instruction;
	}
	/**
	 * Adds a particle effect to this bot.
	 * 
	 * @param effect  The effect to add.
	 */
	public void addEffect(ParticleEffect effect) {
		Stage stage = getStage();
		if (stage!=null) {
			stage.addActor(new ParticleEffectActor(this, effect));
		}
	}
	/**
	 * Sets this bot's speed, that is the number of instructions this bot
	 * can execute in its turn.
	 * 
	 * @param speed
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	/**
	 * Gets this bot's speed.
	 * 
	 * @return
	 */
	public int getSpeed() {
		return speed;
	}
}

package com.violentbits.ageo.game;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.MathUtils;
import com.violentbits.ageo.Constants;
import com.violentbits.ageo.instructions.Instruction;
import com.violentbits.ageo.instructions.InstructionFactory;

/**
 * This class simplifies access to map properties.
 * 
 * A map must have the following layers and properties:
 * 
 * - Map property "goal": this property specifies the goal of a level.
 * @see VictoryConditionFactory for a list of valid goals.
 * 
 * - "Ground" layer: the layer where ground tiles are placed. Should be the lower layer.
 * Tiles in the ground layer may have a "trigger" property.
 * 
 * - "Main" layer: the layer where wall tiles are placed. Properties "block" and "kill" are
 * checked in the tileset to know if a tile let's a bot pass through it in a certain direction,
 * or if a bot gets killed if it tries to pass.
 * 
 * - Optional "Upper" layer: this layer may contain some squares that can't be created with a
 * single layer, and buttons.
 * 
 * - Optional "Objects" layer: may contain some rectangular objects with a "trigger" property.
 * 
 * - "trigger" property: can be placed in the Ground layer or in the Objects layer. When a bot
 * enters a tile that has this property, or gets inside an object with this property, the trigger
 * is activated. A trigger property has the form "<instruction> [<id>]". If there is only an
 * instruction, the bot that activated the trigger immediately executes the specified instruction.
 * If there is an id, all bots with that id execute the instruction.
 */
public class MapController {
	private TiledMap map;

	/**
	 * Assigns a map to the controller. This is the map all
	 * other methods will use.
	 * 
	 * @param map
	 */
	public void setMap(TiledMap map) {
		this.map = map;
	}
	/**
	 * Gets a map's property.
	 * 
	 * @param property  Property name.
	 * @return
	 */
	public String getMapProperty(String property) {
		return map.getProperties().get(property, String.class);
	}
	/**
	 * Gets a property of a tile in the Main layer.
	 * 
	 * @param property  The name of the property.
	 * @param x  x coordinate of the tile.
	 * @param y  y coordinate of the tile.
	 * @return  the value of the property or a blank String if there is no such property in the tile.
	 */
	public String getProperty(String property, float x, float y) {
		int xx = MathUtils.round(x);
		int yy = MathUtils.round(y);
		return getProperty(property, xx, yy);
	}
	/**
	 * Gets a property of a tile in the specified layer. 
	 * 
	 * @param layer  The layer.
	 * @param property  The name of the property.
	 * @param x  x coordinate of the tile.
	 * @param y  y coordinate of the tile.
	 * @return  the value of the property or a blank String if there is no such property in the tile.
	 */
	private String getProperty(TiledMapTileLayer layer, String property, float x, float y) {
		int xx = MathUtils.round(x);
		int yy = MathUtils.round(y);
		return getProperty(layer, property, xx, yy);
	}
	/**
	 * Gets a property of a tile in the Main layer.
	 * 
	 * @param property  The name of the property.
	 * @param x  x coordinate of the tile.
	 * @param y  y coordinate of the tile.
	 * @return  the value of the property or a blank String if there is no such property in the tile.
	 */
	public String getProperty(String property, int x, int y) {
		return getProperty(getMainLayer(), property, x, y);
	}
	/**
	 * Gets a property of a tile in the specified layer. 
	 * 
	 * @param layer  The layer.
	 * @param property  The name of the property.
	 * @param x  x coordinate of the tile.
	 * @param y  y coordinate of the tile.
	 * @return  the value of the property or a blank String if there is no such property in the tile.
	 */
	private String getProperty(TiledMapTileLayer layer, String property, int x, int y) {
		String block = "";
		if (x>=0 && y>=0 && x<layer.getWidth() && y<layer.getHeight()) {
			Cell cell = layer.getCell(x, y);
			if (cell != null) {
				MapProperties properties = cell.getTile().getProperties();
				block = properties.get(property, "", String.class);
			}
		}
		return block;
	}
	/**
	 * Gets a reference to the Main layer.
	 * 
	 * @return
	 */
	private TiledMapTileLayer getMainLayer() {
		return (TiledMapTileLayer) map.getLayers().get("Main");
	}
	/**
	 * Gets a reference to the Ground layer.
	 * 
	 * @return
	 */
	private TiledMapTileLayer getGroundLayer() {
		return (TiledMapTileLayer) map.getLayers().get("Ground");
	}
	/**
	 * Gets a property of an object in the Objects layer.
	 * 
	 * @param property  The name of the property.
	 * @param x  x coordinate of the map, in world coordinates (a tile is a unit).
	 * @param y  y coordinate of the map, in world coordinates (a tile is a unit).
	 * @return  the value of the property if there is a RectangleMapObject that contains
	 * the specified coordinates and has the specified property, or a blank String otherwise.
	 */
	public String getObjectProperty(String property, float x, float y) {
		MapLayer objectLayer = map.getLayers().get("Objects");
		if (objectLayer != null) {
			for (MapObject mapObject : objectLayer.getObjects()) {
				if (mapObject instanceof RectangleMapObject) {
					RectangleMapObject obj = (RectangleMapObject) mapObject;
					if (obj.getRectangle().contains((x+0.5f)*Constants.TILE_SIZE,(y+0.5f)*Constants.TILE_SIZE)) {
						return obj.getProperties().get(property, "", String.class);
					}
				}
			}
		}
		return "";
	}
	/**
	 * Gets the distance in world coordinates (1 tile = 1 unit) between a specified position
	 * and the nearest wall going up.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public float getUpWallDistance(float x, float y) {
		int xx = MathUtils.round(x);
		int yy = MathUtils.round(y);
		TiledMapTileLayer layer = getMainLayer();
		
		while (yy<layer.getHeight()) {
			if (getProperty("block", xx, yy).contains("up"))
				return yy-y;
			yy++;
		}
		return yy-y;
	}
	/**
	 * Gets the distance in world coordinates (1 tile = 1 unit) between a specified position
	 * and the nearest wall going down.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public float getDownWallDistance(float x, float y) {
		int xx = MathUtils.round(x);
		int yy = MathUtils.round(y);
		
		while (yy>=0) {
			if (getProperty("block", xx, yy).contains("down"))
				return y-yy;
			yy--;
		}
		return y-yy;
	}
	/**
	 * Gets the distance in world coordinates (1 tile = 1 unit) between a specified position
	 * and the nearest wall going to the left.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public float getLeftWallDistance(float x, float y) {
		int xx = MathUtils.round(x);
		int yy = MathUtils.round(y);
		
		while (xx>=0) {
			if (getProperty("block", xx, yy).contains("left"))
				return x-xx;
			xx--;
		}
		return x-xx;
	}
	/**
	 * Gets the distance in world coordinates (1 tile = 1 unit) between a specified position
	 * and the nearest wall going to the right.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public float getRightWallDistance(float x, float y) {
		int xx = MathUtils.round(x);
		int yy = MathUtils.round(y);
		TiledMapTileLayer layer = getMainLayer();
		
		while (xx<layer.getWidth()) {
			if (getProperty("block", xx, yy).contains("right"))
				return xx-x;
			xx++;
		}
		return xx-x;
	}
	/**
	 * Gets the distance in world coordinates (1 tile = 1 unit) between a specified position
	 * and the nearest wall moving in the specified rotation degree.
	 * 
	 * @param x
	 * @param y
	 * @param rotation  Walls will only be search in the four main directions. Any other rotation
	 * will be considered as the nearest in the 0, 90, 180, and 170 set.
	 * @return
	 */
	public float getWallDistance(float x, float y, float rotation) {
		rotation = (360+rotation)%360;
		if (MathUtils.isEqual(rotation, 90, 45)) {
			return getLeftWallDistance(x, y);
		} else if (MathUtils.isEqual(rotation, 180, 45)) {
			return getDownWallDistance(x, y);
		} else if (MathUtils.isEqual(rotation, 270, 45)) {
			return getRightWallDistance(x, y);
		} else {
			return getUpWallDistance(x, y);
		}
	}
	/**
	 * Runs a trigger in the specified coordinates, if there is one.
	 * 
	 * @param worldController  A reference to the world controller.
	 * @param bot  The bot that activates the trigger.
	 * @param x  x coordinate of the trigger object.
	 * @param y  y coordinate of the trigger object.
	 */
	public void runTrigger(WorldController worldController, Bot bot, float x, float y) {
		String trigger = getObjectProperty("trigger", x, y);
		if (trigger.equals("")) {
			trigger = getProperty(getGroundLayer(), "trigger", x, y);
		}
		if (!trigger.equals("")) {
			String[] parts = trigger.split(" ");
			Instruction ins = InstructionFactory.instance.getInstruction(parts[0]);
			if (parts.length==1) {
				ins.run(worldController, bot);
			} else {
				int id = Integer.parseInt(parts[1]);
				for (int n=0; n<worldController.getBots().size; n++) {
					Bot b = worldController.getBots().get(n);
					if (b.id == id) {
						ins.run(worldController, b);
					}
				}
			}
		}
	}
}

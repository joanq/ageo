package com.violentbits.ageo.game;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

/**
 * Static methods useful to work with bots.
 */
public class Bots {

	/**
	 * Find the bot that is just in front of another bot.
	 * 
	 * @param worldController  A reference to the world controller.
	 * @param bot  The bot we want to find the facing bot.
	 * @return the bot that is facing our bot, or null if there is none. 
	 */
	public static Bot findFacingBot(WorldController worldController, Bot bot) {
		int x = Math.round(bot.getX());
		int y = Math.round(bot.getY());
		Array<Bot> bots = worldController.getBots();
		Bot facingBot=null;
		
		float rotation = (720+bot.getRotation())%360;
		if (MathUtils.isEqual(rotation, 90f, 1f))
			x-=1;
		else if (MathUtils.isEqual(rotation, 180f, 1f))
			y-=1;
		else if (MathUtils.isEqual(rotation, 270f, 1f))
			x+=1;
		else
			y+=1;
		
		for (Bot otherBot : bots) {
			if (otherBot != bot && MathUtils.isEqual(otherBot.getX(), x, 0.4f) &&
					MathUtils.isEqual(otherBot.getY(), y, 0.4f)) {
				facingBot = otherBot;
			}
		}
		return facingBot;
	}
}

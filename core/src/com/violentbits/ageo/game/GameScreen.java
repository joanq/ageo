package com.violentbits.ageo.game;

import com.badlogic.gdx.Gdx;
import com.violentbits.ageo.AbstractGameScreen;
import com.violentbits.ageo.AgeoGame;
import com.violentbits.ageo.Constants;
import com.violentbits.ageo.SoundController;
import com.violentbits.ageo.io.Assets;
import com.violentbits.ageo.io.GamePreferences;
import com.violentbits.ageo.solver.Solver;

public class GameScreen extends AbstractGameScreen {
	@SuppressWarnings("unused")
	private static final String TAG = GameScreen.class.getName();
	private HudRenderer hudRenderer;
	private WorldController worldController;
	
	private boolean paused;
	
	public GameScreen(AgeoGame game) {
		super(game);
	}

	@Override
	public void show() {
		GamePreferences.instance.load();
		Gdx.graphics.setContinuousRendering(false);
	    Gdx.graphics.requestRendering();
		if (Constants.IS_SOLVER_ACTIVE)
			worldController = new Solver((AgeoGame)game);
		else
			worldController = new DefaultWorldController((AgeoGame) game);
		hudRenderer = new HudRenderer(worldController);
		Gdx.input.setCatchBackKey(true);
		hudRenderer.show();
		SoundController.instance.play(Assets.instance.getMusic("imrobot_v01.mp3"));
	}

	@Override
	public void render(float delta) {
		if (!paused || Constants.IS_SOLVER_ACTIVE) {
			worldController.update(delta);
		}
		// Render game world to screen
		hudRenderer.render(delta);
	}

	@Override
	public void resize(int width, int height) {
		hudRenderer.resize(width, height);
	}

	@Override
	public void pause() {
		paused = true;
	}

	@Override
	public void hide() {
		Gdx.input.setCatchBackKey(false);
		hudRenderer.dispose();
		SoundController.instance.stop();
	}
	
	@Override
	public void resume() {
		super.resume();
		// Only called on Android!
		paused = false;
		hudRenderer.resume();
	}
}

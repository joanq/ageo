package com.violentbits.ageo.game;

public interface WorldListener {
	public void worldChanged(EventType event, Object object);
	
	public enum EventType{
		INIT, WON, RUN, STOP, PAUSE, RESET, ACTOR_ADDED, ACTION
	}
}

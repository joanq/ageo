package com.violentbits.ageo;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.violentbits.ageo.io.GamePreferences;

/**
 * Singleton class used to play sounds and music. All methods take into account
 * user preferences.
 */
public class SoundController {
	/**
	 * Global instance of this class.
	 */
	public static final SoundController instance = new SoundController();
	private Music music;
	
	private SoundController() {}
	
	/**
	 * Plays a sound at maximum volume (1), default pitch (1), and default pan (0).
	 * 
	 * @param sound
	 */
	public void play(Sound sound) {
		play(sound, 1f, 1f, 0f);
	}
	/**
	 * Plays a sound at the specified volume, default pitch (1), and default pan (0).
	 * 
	 * @param sound
	 * @param volume  A number between 0 and 1.
	 */
	public void play(Sound sound, float volume) {
		play(sound, volume, 1f, 0f);
	}
	/**
	 * Plays a sound at the specified volume, pitch and pan.
	 * 
	 * @param sound
	 * @param volume  A number between 0 and 1.
	 * @param pitch  A number between 0.5 and 2. 1 is normal speed, >1 is faster and <1 is slower.
	 * @param pan  A number between -1 and 1. 0 is center.
	 */
	public void play(Sound sound, float volume, float pitch, float pan) {
		if (GamePreferences.instance.sound) {
			sound.play(GamePreferences.instance.soundVol * volume, pitch, pan);
		}
	}
	/**
	 * Plays a music at maximum volume in a loop.
	 * 
	 * @param music
	 */
	public void play(Music music) {
		stop();
		this.music = music;
		if (GamePreferences.instance.music) {
			music.setLooping(true);
			music.setVolume(GamePreferences.instance.musicVol);
			music.play();
		}
	}
	/**
	 * Stops playing music.
	 */
	public void stop() {
		if (this.music!=null) {
			music.stop();
			music=null;
		}
	}
	/**
	 * Call this whenever user preferences changes in order to start
	 * or stop playing music if necessary.
	 */
	public void onSettingsUpdated () {
		if (music != null) {
			music.setVolume(GamePreferences.instance.musicVol);
			if (GamePreferences.instance.music) {
				if (!music.isPlaying())
					music.play();
			} else {
				music.pause();
			}
		}
	}
}

package com.violentbits.ageo.scene;

import static com.violentbits.ageo.Constants.ACTION_TIME_FAST;
import static com.violentbits.ageo.scene.ActionsEx.moveCameraAligned;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.violentbits.ageo.Constants;
import com.violentbits.ageo.MenuScreen;
import com.violentbits.ageo.SoundController;
import com.violentbits.ageo.io.Assets;
import com.violentbits.ageo.io.GamePreferences;
import com.violentbits.ageo.io.Profile;

public class GameChooser extends Group {
	private MenuScreen menuScreen;
	private Table table;
	private Skin skin;

	public GameChooser(MenuScreen menuScreen) {
		super();
		this.menuScreen = menuScreen;
		this.skin = menuScreen.getSkin();
		init();
	}
	
	private void addLoadButtonListeners(final TextButton loadButton, final TextField textField, final TextButton deleteButton, final int pos) {
		final Profile profile = GamePreferences.instance.getProfile(pos);
		loadButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				SoundController.instance.play(Assets.instance.getSound("select.wav"));
				if (profile.getName().equals("")) {
					profile.setName(textField.getText());
				}
				GamePreferences.instance.setCurrentProfile(pos);
				GamePreferences.instance.save();
				textField.setDisabled(true);
				deleteButton.setDisabled(false);
				loadButton.setText("Load");
				menuScreen.getMainMenu().refresh();
				returnToMenu();
			}
		});
	}
	
	private void addTextFieldListeners(final TextField textField, final TextButton loadButton) {
		textField.addListener(new InputListener() {
			@Override
			public boolean keyUp(InputEvent event, int keyCode) {
				loadButton.setDisabled(
						textField.getText().trim().equals("") || textField.getText().trim().equals("Your name"));
				event.stop();
				return true;
			}

			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				textField.selectAll();
				GameChooser.this.addAction(
						ActionsEx.moveCameraAligned(
								Align.bottom, 0, -getStage().getHeight()/2f+GameChooser.this.getHeight(), Constants.ACTION_TIME_FAST));
				event.stop();
				return true;
			}
			
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				
			}
		});
	}
	
	private void addDeleteButtonListeners(final TextButton deleteButton, final TextField textField, final TextButton loadButton, final int pos) {
		final Profile profile = GamePreferences.instance.getProfile(pos);
		deleteButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				menuScreen.pushActor(GameChooser.this);
				SoundController.instance.play(Assets.instance.getSound("select.wav"));
				ConfirmDialog confirmDialog = menuScreen.getConfirmDialog();
				confirmDialog.setQuestion("Delete "+textField.getText()+" profile?");
				confirmDialog.setConfirmListener(new ConfirmListener() {
					@Override
					public void confirmed() {
						if (GamePreferences.instance.getCurrentProfile().getNProfile() == pos) {
							GamePreferences.instance.setCurrentProfile(-1);
						}
						profile.delete();
						textField.setText("Your name");
						textField.setDisabled(false);
						deleteButton.setDisabled(true);
						loadButton.setText("Create");
						loadButton.setDisabled(true);
						menuScreen.getMainMenu().refresh();
					}
				});
				confirmDialog.addAction(moveCameraAligned(Align.center, ACTION_TIME_FAST));
			}
		});
	}
	
	private TextButton createBackButton() {
		TextButton backButton = new TextButton("<", skin);
		backButton.setSize(50f, 50f);
		backButton.setPosition(-100f, -100f);
		backButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				returnToMenu();
			}
		});
		return backButton;
	}
	
	private void createGameSlot(final int pos) {
		final Profile profile = GamePreferences.instance.getProfile(pos);
		final TextField textField = new TextField("Your name", skin);
		final TextButton loadButton = new TextButton("Create", skin);
		loadButton.getStyle().disabled = skin.newDrawable("default-round", Color.GRAY);
		final TextButton deleteButton = new TextButton("Delete", skin);

		table.add(textField).width(200f);
		table.add(loadButton).width(100f);
		table.add(deleteButton).width(100f).row();
		addTextFieldListeners(textField, loadButton);
		addLoadButtonListeners(loadButton, textField, deleteButton, pos);
		addDeleteButtonListeners(deleteButton, textField, loadButton, pos);

		if (profile==null || profile.getName().equals("")) {
			// Saved game doesn't exist
			deleteButton.setDisabled(true);
			loadButton.setDisabled(true);
		} else {
			textField.setDisabled(true);
			textField.setText(profile.getName());
			loadButton.setText("Load");
		}
	}

	private void init() {
		table = new Table(skin);
		table.defaults().spaceBottom(20f).spaceLeft(10f);
		Label titleLbl = new Label("Profiles", skin);
		table.add(titleLbl).colspan(3).center().row();
		for (int i = 0; i < Constants.GAME_SLOTS; i++) {
			createGameSlot(i);
		}
		table.pack();
		table.setPosition(0f, 0f);
		addActor(table);
		addActor(createBackButton());
		setWidth(table.getWidth());
		setHeight(table.getHeight());
	}
	
	private void returnToMenu() {
		menuScreen.back();
	}
}

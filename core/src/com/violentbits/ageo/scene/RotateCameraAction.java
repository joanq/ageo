package com.violentbits.ageo.scene;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;

public class RotateCameraAction extends TemporalAction {
	private static final Vector3 axis = new Vector3(0f,0f,1f);
	private boolean absolute;
	private float rotation;
	private float endRotation;
	private Vector3 center = new Vector3();
	private Camera cam;
	
	private float totalRotation;

	@Override
	protected void begin () {
		rotation=0f;
		totalRotation=0f;
		cam = target.getStage().getCamera();
		if (!absolute) {
			center.x = target.getX();
			center.y = target.getY();
		} else {
			center.x = cam.position.x;
			center.y = cam.position.y;
		}
	}
	
	@Override
	protected void update(float percent) {
		rotation = endRotation*percent - totalRotation;
		totalRotation+=rotation;
		cam.rotateAround(center, axis, rotation);
	}
	
	@Override
	public void reset () {
		super.reset();
		absolute=false;
	}

	public Camera getCamera() {
		return cam;
	}
	
	public void setCamera(Camera cam) {
		this.cam = cam;
	}

	public boolean isAbsolute() {
		return absolute;
	}

	public void setAbsolute(boolean absolute) {
		this.absolute = absolute;
	}

	public float getEndRotation() {
		return endRotation;
	}

	public void setEndRotation(float endRotation) {
		this.endRotation = endRotation;
	}
}

package com.violentbits.ageo.scene;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;
import com.violentbits.ageo.io.Assets;

public class LaserActor extends Actor {
	@SuppressWarnings("unused")
	private static final String TAG = LaserActor.class.getName();
	private static final float scaleAlpha = 0.9f;
	private WorldController worldController;
	private Color colorGlow = new Color(Color.RED);
	private Color colorBackground = new Color(Color.FIREBRICK);
	private float accDelta;
	
	private AtlasRegion startBackground = Assets.instance.get("laser-start-b");
	private AtlasRegion startOverlay = Assets.instance.get("laser-start-o");
	private AtlasRegion midBackground = Assets.instance.get("laser-mid-b");
	private AtlasRegion midOverlay = Assets.instance.get("laser-mid-o");
	private AtlasRegion animation = Assets.instance.get("laser-animation");
	
	private float startLength = 1f;
	private float midLength;
	
	private float startLengthX;
	private float startLengthY;
	
	public LaserActor(WorldController worldController) {
		super();
		this.worldController = worldController;
	}
	
	public void activate(Actor emissor) {
		setPosition(emissor.getX(), emissor.getY());
		setOrigin(emissor.getOriginX(), emissor.getOriginY());
		setRotation(emissor.getRotation());
		setSize(1f, Math.round(1f+worldController.getMapController().getWallDistance(getX(), getY(), getRotation())));
		setScale(emissor.getScaleX(), emissor.getScaleY());
		midLength = getHeight()-startLength;
		
		startLengthX = -startLength*MathUtils.sinDeg(getRotation());
		startLengthY = startLength*MathUtils.cosDeg(getRotation());
		
		deactivateBots();
	}
	
	private void deactivateBots() {
		Bot bot;
		int x = Math.round(getX());
		int y = Math.round(getY());
		int rotation = Math.round(getRotation());
		
		for (int i=0; i<getHeight(); i++) {
			x-=Math.round(MathUtils.sinDeg(rotation));
			y+=Math.round(MathUtils.cosDeg(rotation));
			//Gdx.app.debug(TAG, "x: "+x+" y: "+y+" rot: "+rotation);
			bot = worldController.getBotAtPosition(x, y);
			if (bot!=null) {
				//Gdx.app.debug(TAG, "Hit x: "+bot.getX()+" y: "+bot.getY());
				bot.setActive(false);
			}
		}
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		
		int srcFunction = batch.getBlendSrcFunc();
		int dstFunction = batch.getBlendDstFunc();
		batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
		float alpha = 1f - (float)Math.pow(accDelta / worldController.actionTime, 2.0);
		colorGlow.a = alpha;
		colorBackground.a = alpha;
		batch.setColor(colorBackground);
		batch.draw(startBackground, getX(), getY(), getOriginX(), getOriginY(),
				getWidth(), startLength, getScaleX(), getScaleY(), getRotation());
		batch.setColor(colorGlow);
		batch.draw(startOverlay, getX(), getY(), getOriginX(), getOriginY(),
				getWidth(), startLength, getScaleX(), getScaleY(), getRotation());
		batch.setColor(colorBackground);
		if (getHeight()>1) {
			batch.draw(midBackground, getX()+startLengthX, getY()+startLengthY, getOriginX(), getOriginY(),
					getWidth(), midLength, getScaleX(), getScaleY(), getRotation());
			batch.setColor(colorGlow);
			batch.draw(midOverlay, getX()+startLengthX, getY()+startLengthY, getOriginX(), getOriginY(),
					getWidth(), midLength, getScaleX(), getScaleY(), getRotation());
			colorGlow.a=1;
			for (int i=1; i<getHeight(); i++) {
				batch.setColor(colorGlow);
				batch.draw(animation, getX()-i*MathUtils.sinDeg(getRotation()), getY()+i*MathUtils.cosDeg(getRotation()),
						getOriginX(), getOriginY(),	getWidth(), 1f, getScaleX(), getScaleY(), getRotation());
				colorGlow.a *= scaleAlpha;
			}
		}
		batch.setBlendFunction(srcFunction, dstFunction);
	}
	
	@Override
	public void act(float delta) {
		accDelta+=delta;
		if (accDelta>worldController.actionTime) {
			colorGlow.a = 1f;
			colorBackground.a = 1f;
			accDelta = 0;
			remove();
		}
	}
}

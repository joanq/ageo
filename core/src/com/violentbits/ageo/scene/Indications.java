package com.violentbits.ageo.scene;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.violentbits.ageo.Constants;
import com.violentbits.ageo.io.Assets;

public class Indications {
	private Skin skin;
	private Stage stage;
	private Array<Label> labels = new Array<Label>();
	private Array<Image> lines = new Array<Image>();
	
	public Indications(Stage stage, Skin skin) {
		this.stage = stage;
		this.skin = skin;
	}
	
	public enum Dir{UP, RIGHT, DOWN, LEFT}
	
	public void addLabelAndLine(Actor target, String text, Dir dir, float length) {
		float w, h;
		if (dir==Dir.UP || dir==Dir.DOWN) {
			w=3f;
			h=length;
		} else {
			h=3f;
			w=length;
		}
		Vector2 pos = target.localToStageCoordinates(new Vector2(0,0)); 
		Image line = new Image(Assets.instance.getNinePatch("line"));
		Label label = new Label(text, skin);
		line.setSize(w, h);
		line.setColor(Constants.BACKGROUND_BLUE);
		
		switch (dir) {
		case UP:
			line.setPosition(pos.x+(target.getWidth()-line.getWidth())/2f, pos.y-h);
			label.setPosition(line.getX()+5f, line.getY());
			break;
		case DOWN:
			line.setPosition(pos.x+(target.getWidth()-line.getWidth())/2f, pos.y+target.getHeight());
			label.setPosition(line.getX()+5f, line.getY()+line.getHeight()-label.getHeight());
			break;
		case LEFT:
			line.setPosition(pos.x+target.getWidth(), pos.y+(target.getHeight()-line.getHeight())/2f);
			label.setPosition(line.getX()+line.getWidth()-label.getWidth(), line.getY()+5f);
			break;
		case RIGHT:
			line.setPosition(pos.x-w, pos.y+(target.getHeight()-line.getHeight())/2f);
			label.setPosition(line.getX(), line.getY()+5f);
			break;
		}
		stage.addActor(label);
		labels.add(label);
		stage.addActor(line);
		lines.add(line);
	}
	
	public void removeAll() {
		for (Label label : labels) {
			label.remove();
		}
		for (Image line : lines) {
			line.remove();
		}
	}
}

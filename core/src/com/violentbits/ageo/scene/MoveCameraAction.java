package com.violentbits.ageo.scene;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;
import com.badlogic.gdx.utils.Align;

/** Moves the camera from its current position to an actor's position. */
public class MoveCameraAction extends TemporalAction {
	private float startX, startY;
	private float endX, endY;
	private int alignment = Align.bottomLeft;
	private Camera cam;
	private float dx, dy;

	@Override
	protected void begin () {
		endX = target.getX(alignment)+dx;
		endY = target.getY(alignment)+dy;
		cam = target.getStage().getCamera();
		startX = cam.position.x;
		startY = cam.position.y;
	}

	@Override
	protected void update (float percent) {
		cam.position.x = startX + (endX - startX) * percent;
		cam.position.y = startY + (endY - startY) * percent;
	}

	@Override
	public void reset () {
		super.reset();
		alignment = Align.bottomLeft;
		dx=dy=0f;
	}

	public void setPosition (float x, float y) {
		endX = x;
		endY = y;
	}

	public void setPosition (float x, float y, int alignment) {
		endX = x;
		endY = y;
		this.alignment = alignment;
	}

	public float getX () {
		return endX;
	}

	public void setX (float x) {
		endX = x;
	}

	public float getY () {
		return endY;
	}

	public void setY (float y) {
		endY = y;
	}

	public int getAlignment () {
		return alignment;
	}

	public void setAlignment (int alignment) {
		this.alignment = alignment;
	}
	
	public Camera getCamera() {
		return cam;
	}
	
	public void setCamera(Camera cam) {
		this.cam = cam;
		startX = cam.position.x;
		startY = cam.position.y;
	}
	
	public float getDx() {
		return dx;
	}
	
	public void setDx(float dx) {
		this.dx = dx;
	}
	
	public float getDy() {
		return dy;
	}
	
	public void setDy(float dy) {
		this.dy = dy;
	}
}

package com.violentbits.ageo.scene;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.violentbits.ageo.game.Bot;

public class ParticleEffectActor extends Actor {
	private Bot bot;
	private ParticleEffect effect;

	public ParticleEffectActor(Bot bot, ParticleEffect effect) {
		this.bot = bot;
		this.effect = effect;
		effect.start();
		effect.setPosition(bot.getX()+0.5f, bot.getY()+0.5f);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		effect.draw(batch);
	}

	@Override
	public void act(float delta) {
		super.act(delta);
		if (effect.isComplete()) {
			remove();
		} else {
			for (ParticleEmitter emitter : effect.getEmitters()) {
				emitter.getAngle().setHigh(bot.getRotation()+90);
				emitter.getAngle().setLow(bot.getRotation()+90);
				emitter.getRotation().setHigh(bot.getRotation()+90);
				emitter.getRotation().setLow(bot.getRotation()+90);
			}
			effect.update(delta);
		}
	}

	public ParticleEffect getEffect() {
		return effect;
	}
}

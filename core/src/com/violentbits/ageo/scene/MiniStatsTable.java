package com.violentbits.ageo.scene;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.violentbits.ageo.Constants;
import com.violentbits.ageo.io.Assets;
import com.violentbits.ageo.io.GamePreferences;
import com.violentbits.ageo.io.Profile;

public class MiniStatsTable extends Table {
	private static final Color[] colors = {Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN}; 
	private Image imgInst, imgNSteps;
	
	public MiniStatsTable(String levelName) {
		defaults().size(Constants.INSTRUCTION_ICON_SIZE/2f);
		imgInst = new Image(Assets.instance.get("gears"));
		imgInst.setColor(Constants.PURPLE);
		imgNSteps = new Image(Assets.instance.get("clock"));
		imgNSteps.setColor(Constants.PURPLE);
		add(imgInst);
		add(imgNSteps);
		row();
		
		Profile profile = GamePreferences.instance.getCurrentProfile();
		int instStars = profile.getBestInstStars(levelName);
		int stepsStars = profile.getBestStepsStars(levelName);
		if (instStars>=0) {
			imgInst.addAction(Actions.color(colors[instStars], 1f));
		}
		if (stepsStars>=0) {
			imgNSteps.addAction(Actions.color(colors[stepsStars], 1f));
		}
		pack();
	}
}

package com.violentbits.ageo.solver;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.game.WorldController;

public class InstantlyBot extends Bot {

	public InstantlyBot(String textureName, Color flagColor) {
		super(textureName, flagColor);
	}

	@Override
	public void reset(WorldController worldController) {
		super.reset(worldController);
		setPosition(initialX, initialY);
		setRotation(initialRotation);
	}
	
	@Override
	public void addAction(Action action) {
		throw new UnsupportedOperationException("InstantlyBot can't have actions");
	}
	
	@Override
	public void addEffect(ParticleEffect effect) {
		// Ignore effect
	}
}

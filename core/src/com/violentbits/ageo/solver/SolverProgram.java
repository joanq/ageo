package com.violentbits.ageo.solver;

import com.violentbits.ageo.apparat.Cell;
import com.violentbits.ageo.apparat.Program;
import com.violentbits.ageo.game.Bot;
import com.violentbits.ageo.instructions.Instruction;
import com.violentbits.ageo.instructions.InstructionFactory;

public class SolverProgram {
	@SuppressWarnings("unused")
	private static final String TAG = SolverProgram.class.getName();
	private Bot bot;
	private Program program;
	private int[] setPos;
	private int programSize;
	private int pos;
	private int firstPos=-1;
	
	public SolverProgram(Bot bot, int firstPos) {
		this(bot);
		this.firstPos=firstPos;
	}
	
	public SolverProgram(Bot bot) {
		this.bot = bot;
		program = bot.getProgram();
		setPos = new int[program.getCells().size];
	}
	
	public void setProgramSize(int programSize) {
		this.programSize = programSize;
		resetProgram();
	}
	
	public void resetProgram() {
		Instruction ins = bot.getInstructionSet().getCells().get(0).getInstruction();
		program.clear();
		if (firstPos<0) {
			for (pos=0; pos<programSize; pos++) {
				setPos[pos]=0;
				program.addInstruction(pos, InstructionFactory.instance.getInstruction(ins));
			}
		} else {
			setPos[0]=firstPos;
			program.addInstruction(pos, InstructionFactory.instance.getInstruction(
					bot.getInstructionSet().getCells().get(firstPos).getInstruction()));
			for (pos=1; pos<programSize; pos++) {
				setPos[pos]=0;
				program.addInstruction(pos, InstructionFactory.instance.getInstruction(ins));
			}
		}
		pos=programSize-1;
	}
	
	public boolean computeNextCandidate() {
		boolean run=false;
		
		if (firstPos<0) {
			while (pos>=0 && setPos[pos]==bot.getInstructionSet().getCells().size-1) {
				Instruction ins = bot.getInstructionSet().getCells().get(0).getInstruction();
				program.setInstruction(pos, InstructionFactory.instance.getInstruction(ins));
				setPos[pos]=0;
				pos--;
			}
			if (pos>=0) {
				setPos[pos]++;
				Instruction ins = bot.getInstructionSet().getCells().get(setPos[pos]).getInstruction();
				program.setInstruction(pos, InstructionFactory.instance.getInstruction(ins));
				pos=programSize-1;
				run=true;
			}
		} else {
			while (pos>=1 && setPos[pos]==bot.getInstructionSet().getCells().size-1) {
				Instruction ins = bot.getInstructionSet().getCells().get(0).getInstruction();
				program.setInstruction(pos, InstructionFactory.instance.getInstruction(ins));
				setPos[pos]=0;
				pos--;
			}
			if (pos>=1) {
				setPos[pos]++;
				Instruction ins = bot.getInstructionSet().getCells().get(setPos[pos]).getInstruction();
				program.setInstruction(pos, InstructionFactory.instance.getInstruction(ins));
				pos=programSize-1;
				run=true;
			}
		}
		return run;
	}
	
	public void increaseProgramSize() {
		programSize++;
		resetProgram();
	}
	
	public void decreaseProgramSize() {
		programSize--;
		resetProgram();
	}
	
	public void restoreBackup() {
		program.restoreBackup();
	}
	
	public String programToString() {
		String s="";
		for (Cell cell: program.getCells()) {
			if (!cell.isEmpty())
				s+=cell.getInstruction().getName()+" ";
		}
		return s;
	}
	
	public Bot getBot() {
		return bot;
	}
	
	public Program getProgram() {
		return program;
	}
	
	public int getProgramSize() {
		return programSize;
	}
}

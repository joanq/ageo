package com.violentbits.ageo.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.violentbits.ageo.AgeoGame;
import com.violentbits.ageo.Constants;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		if (Constants.IS_SOLVER_ACTIVE) {
			config.vSyncEnabled = false; // Setting to false disables vertical sync
			config.foregroundFPS = 0;
			config.backgroundFPS = 0;
		}
		new LwjglApplication(new AgeoGame(), config);
	}
}

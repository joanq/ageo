#/bin/bash

#blau puntes 939cc0
originals[0]=939cc0
destins[0]=027a90
#blau fons a1cad4
originals[1]=a1cad4
destins[1]=0b8eac
#blau vores 4a4f9b
originals[2]=4a4f9b
destins[2]=4a4f9b
#vermell ratlles i vermell vores df213f
originals[3]=df213f
destins[3]=ff0080
#taronja ratlles ed8024
originals[4]=ed8024
destins[4]=260099
#taronja puntes da4320
originals[5]=da4320
destins[5]=dd0090
#blau relleu d3e9e1
originals[6]=d3e9e1
destins[6]=2baecc
#marró puntes 542146
originals[7]=542146
destins[7]=542146
#groc relleu f5b62a
originals[8]=f5b62a
destins[8]=4020cc
#rosa relleu ff34c3
originals[9]=ff34c3
destins[9]=ff3080
#marró vores cf3620
originals[10]=cf3620
destins[10]=ee1080
#vermell puntes ba2a34
originals[11]=ba2a34
destins[11]=ff20a0
#fons i negre puntes 1f1d2c
originals[12]=1f1d2c
destins[12]=010133

#
#botó:
#vermell vora i vermell puntes a72152
originals[13]=a72152
destins[13]=a72152
#vermell fons df213f *
#rosa relleu f42a82
originals[14]=f42a82
destins[14]=f42a82
#rosa dibuix ff34c3 *

#graella
#blanc f3fff4
originals[15]=f3fff4
destins[15]=f3fff4
#blau intersecció 6f76ae
originals[16]=6f76ae
destins[16]=6f76ae

fitxer_orig="$1"
fitxer_desti="$2"

len=${#originals[@]}

cp $fitxer_orig $fitxer_desti
for (( i=0; i<${len}; i++ ));
do
  echo \#${originals[$i]} \#${destins[$i]}
  convert $fitxer_desti -fill \#${destins[$i]} -opaque \#${originals[$i]} $fitxer_desti
done

